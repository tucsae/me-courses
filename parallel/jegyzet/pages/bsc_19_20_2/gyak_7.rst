7. gyakorlat
============

.. note::

    Ezen gyakorlat témája és feladatai legalább annyira kapcsolódnak a párhuzamos programozáshoz, mint az operációs rendszerek, rendszeradminisztrátori feladatok és a C programozás témaköréhez. Ebből kifolyólag nem kell megijedni, hogy ha a Párhuzamos algoritmusok tárgyhoz kevésbé kötődő ismereteket is sikerül gyakorolni, bővíteni közben.


Környezet kiválasztása
----------------------

A PVM alapvetően UNIX-szerű rendszereken használható. A cél az, hogy a gyakorlati diasorban és a forráskódok között szereplő példákat le sikerüljön futtatni, és meg sikerüljön érteni azok működését.

Ki kellene választani egy szimpatikus GNU/Linux disztribúciót (vagy más UNIX-szerű operációs rendszert).
A következőket inkább csak példaként említem néhány észrevétellel kiegészítve.

* *Debian*: A ``stretch`` közvetlenül elérhető a ``pvm`` és a ``libpvm3`` mint csomag.
* *Ubuntu*: Többféle asztali környezettel, telepítési opcióval elérhető. Kicsit felhasználóbarátabb, mint a Debian.
* *Linux Mint*: Szintén Debian alapú, nagyon elterjedt.
* *Arch Linux*: Inkább csak a bátrabbaknak ajánlom. Ehhez a csomaglista alapján úgy látom, hogy a PVM-et és a library-t forrásból kell fordítani.
* *OpenSUSE*, *CentOS*: RedHat alapú disztribúciók. Megbízhatóak, elterjedtek, jól támogatottak.

További disztribúciókért ezen az oldalon érdemes szétnézni: `https://distrowatch.com <https://distrowatch.com>`_.


Telepítés
---------

Amennyiben a hardveres erőforrások lehetővé teszik, a UNIX rendszert elegendő VirtualBox-ban telepíteni (`https://www.virtualbox.org/ <https://www.virtualbox.org/>`_).


Feladat
-------

* A gyakorlati diasor 152-198. diái közötti részt el kellene olvasni.
* Ebben szerepel, hogy hogyan konfigurálható a PVM.
* A mintakódok elérhetők innen: `parh_alg_sample_codes.zip <https://www.uni-miskolc.hu/~matolaj/parh_alg_sample_codes.zip>`_.
* Le kellene fordítani és le kellene futtatni a ``hello`` nevű példát.

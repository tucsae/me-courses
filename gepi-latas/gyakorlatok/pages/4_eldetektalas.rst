4. Éldetektálás
===============

* https://www.bogotobogo.com/python/OpenCV_Python/python_opencv3_Image_Gradient_Sobel_Laplacian_Derivatives_Edge_Detection.php
* https://www.ics.uci.edu/~majumder/DIP/classes/EdgeDetect.pdf


Feldolgozási lépések
--------------------

Egy tipikus éldetektálási folyamat a következő lépésekből áll.

* Simítás
* Élkiemelés
* Küszöbölés
* Blob-ok detektálása

.. todo::

   Készítsünk egy programot, amelyik egy képen az előzőleg említett lépéseket hajtja végre!

.. todo::

   Szedjük össze az összes előforduló szabad paramétert!

.. todo::

   Adjuk megadni az elvárt kimenetet, majd próbáljuk meg becsülni az optimális paramétereket!


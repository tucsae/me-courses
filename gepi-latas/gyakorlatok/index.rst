Gépi látás
==========

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   pages/bevezetes
   pages/1_szinterek
   pages/2_hisztogram
   pages/3_kuszoboles
   pages/4_eldetektalas
   pages/5_osztalyozas
   pages/6_szegmentalas

.. pages/7_alakzatfelismeres

